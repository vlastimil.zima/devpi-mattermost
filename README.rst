devpi-mattermost: Mattermost notification plugin for devpi-server
=================================================================

Installation
------------

``devpi-mattermost`` needs to be installed alongside ``devpi-server``.

You can install it with::

    pip install devpi-mattermost

For ``devpi-server`` there is no configuration needed, as it will automatically discover the plugin through calling hooks using the setuptools entry points mechanism.

Details about configuration below.

Configuration
-------------

devpi-mattermost can trigger Mattermost notifications upon package upload.

    devpi index /testuser/dev mattermost_webhook=https://mattermost.example/hooks/HOOK_ID mattermost_user=devpi mattermost_channel=devpi

Environment Variables:

Optionally, you can pass environment variables to configure the plugin.

- ``MATTERMOST_WEBHOOK`` to adjust the Mattermost hook URL used. Defaults to the devpi mattermost_hook value above. (Note: mattermost_hook provided by devpi takes precedence. Setting both will default to the value specified in devpi)
- ``MATTERMOST_USER`` to adjust the Mattermost username used. Not provided if unset.
- ``MATTERMOST_CHANNEL`` to adjust the Mattermost username used. Not provided if unset.

Uninstall
---------

Remove the index configuration:

    devpi index /testuser/dev mattermost_webhook-= mattermost_user-= mattermost_channel-=

Aknowledgement
--------------

Based on devpi-slack https://github.com/innoteq/devpi-slack
